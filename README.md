# Arike Database Command Line Interface

[![pipeline status](https://gitlab.com/arikedb1/arike-database-command-line-interface/badges/main/pipeline.svg)](https://gitlab.com/arikedb1/arike-database-command-line-interface/-/commits/main)
[![coverage report](https://gitlab.com/arikedb1/arike-database-command-line-interface/badges/main/coverage.svg)](https://gitlab.com/arikedb1/arike-database-command-line-interface/-/commits/main)
[![Latest Release](https://gitlab.com/arikedb1/arike-database-command-line-interface/-/badges/release.svg)](https://gitlab.com/arikedb1/arike-database-command-line-interface/-/releases)
