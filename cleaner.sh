#!/bin/bash

find ./arikedb_cli -type f -name "*.c" -delete
find ./arikedb_cli -type f -name "*.cpp" -delete
find ./arikedb_cli -type f -name "*.so" -delete
rm -rf parts prime stage build

exit 0
