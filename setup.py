#!/usr/bin/python3
from setuptools import find_packages, setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


with open('arikedb_cli/__init__.py', 'r') as f:
    version = list(
        filter(lambda line: "__version__ =" in line, f.readlines())
    )[0].split("\"")[1]

setup(
    name="arikedb_cli",
    version=version,
    description="Arikedb Python Command Line Interface",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Alejandro Alfonso",
    author_email="alejandroalfonso1994@gmail.com",
    packages=find_packages(exclude=['tests', 'demo']),
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "arikedb-cli = arikedb_cli.launcher.main:main"
        ]
    },
    install_requires=[r.strip() for r in open("requirements.txt").readlines()]
)
