import base64
import json
import os.path
import socket
import ssl
from json import JSONDecodeError
from typing import Optional

from arikedb_tools.sockets import Socket
from arikedb_tools.tables import format_table


class ArikedbClient:

    def __init__(self, host: str = "localhost", port: int = 6923,
                 cert_file: Optional[str] = None):
        """RTDB Client constructor"""
        self.host = host
        self.port = port
        self.cert = cert_file
        self.socket: Optional[Socket] = None
        self.ssl_ctx = None
        self.subscribed = None
        self.reading_sub = False
        self.responses = {}

    def connect(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        if self.cert:
            self.ssl_ctx = ssl.create_default_context(
                ssl.Purpose.SERVER_AUTH
            )
            self.ssl_ctx.load_verify_locations(cafile=self.cert)
            sock = self.ssl_ctx.wrap_socket(
                sock, server_hostname=self.host
            )
        self.socket = Socket(sock)

        self.socket.socket.connect((self.host, self.port))

    def send_command(self, cmd: str) -> dict:
        if cmd.upper().startswith("LOAD_LICENSE "):
            header = "LOAD_LICENSE "
            file = cmd[len(header):].strip()
            if os.path.isfile(file):
                with open(file, "rb") as f:
                    data = f.read()
                    if not data:
                        return {
                            "status": 1,
                            "msg": "Empty file"
                        }
                    else:
                        data = base64.b64encode(data).decode()
                        cmd = "LOAD_LICENSE " + data
            else:
                return {
                    "status": 1,
                    "msg": "Invalid license file path"
                }

        self.socket.send(cmd)
        response = json.loads(self.socket.receive_n()[0])
        return response

    def disconnect(self) -> dict:
        return self.send_command("EXIT")

    def handle_response(self, response: dict) -> Optional[int]:

        msg = response.get("msg")
        status = response.get("status")
        databases = response.get("databases")
        roles = response.get("roles")
        users = response.get("users")
        values = response.get("values")
        sub_id = response.get("sub_id")
        variables = response.get("variables")

        if status == 0:
            for db in databases or []:
                print(f" - {db}")
            for role in roles or []:
                allowed = ' '.join([cmd.upper().replace('COMMAND', '')
                                    for cmd in role['allowed_cmd']])
                print(f" - {role['name']}")
                print(f"   > {allowed}")
            for user in users or []:
                print(f" - {user['username']} : {user['role']}")
            for val in values or []:
                if all(isinstance(x, list) for x in val):
                    for h_ in val:
                        meta = ' '.join(f"{k}={v}" for k, v in h_[3].items())
                        print(f"{h_[1]} - {h_[0]}: {h_[2]} {meta}")
                else:
                    meta = ' '.join(f"{k}={v}" for k, v in val[3].items())
                    print(f"{val[1]} - {val[0]}: {val[2]} {meta}")
            for var in variables or []:
                print(f" - {var}")
            if sub_id:
                while True:
                    try:
                        sub_resp = json.loads(self.socket.receive_n()[0])
                        if sub_resp.get("sub_id") == "-1":
                            break
                        if "values" in sub_resp:
                            for val in sub_resp.get("values"):
                                meta = [f"{k}={v}" for k, v in val[3].items()]
                                meta = ' '.join(meta)
                                print(f"{val[1]} - {val[0]}: {val[2]} {meta}")
                    except KeyboardInterrupt:
                        resp = self.send_command(f"UNSUBSCRIBE {sub_id}")
                        if resp.get("sub_id") == "-1":
                            print("\r", end="")
                            break
                    except (JSONDecodeError, TypeError):
                        pass
            if msg:
                try:
                    msg = json.loads(msg)
                    if "headers" in msg:
                        print(format_table("Arikedb Config", msg["headers"],
                                           msg["tab"], msg["cell_len"]))
                except JSONDecodeError:
                    print(msg)
                if msg == "Bye":
                    return 1
        else:
            print(msg)
