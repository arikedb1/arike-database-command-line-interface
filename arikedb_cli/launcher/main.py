import time
from os import path, name, environ
from argparse import ArgumentParser
from getpass import getpass
try:
    import pyreadline3
    _ = pyreadline3
except ImportError:
    pyreadline3 = None

if name == "nt":
    # Windows support
    from pyreadline3 import Readline
    readline = Readline()
else:
    import readline

from arikedb_cli.arikedb.arikedb import ArikedbClient
from arikedb_cli import __version__


def completer(text: str, state):
    options = [
        "SET",
        "set",
        "RM",
        "rm",
        "SET_EVENT",
        "set_event",
        "GET",
        "get",
        "PGET",
        "pget",
        "SUBSCRIBE",
        "SUB",
        "subscribe",
        "sub",
        "PSUBSCRIBE",
        "PSUB",
        "psubscribe",
        "psub",
        "UNSUBSCRIBE",
        "UNSUB",
        "unsubscribe",
        "unsub",
        "VARIABLES",
        "VARS",
        "variables",
        "vars",
        "AUTH",
        "auth",
        "ADD_ROLE",
        "add_role",
        "DEL_ROLE",
        "del_role",
        "ADD_USER",
        "add_user",
        "DEL_USER",
        "del_user",
        "SHOW",
        "show",
        "ADD_DATABASE",
        "ADD",
        "add_database",
        "add",
        "DEL_DATABASE",
        "DEL",
        "del_database",
        "del",
        "USE_DATABASE",
        "USE",
        "use_database",
        "use",
        "EXIT",
        "exit",
        "CLOSE",
        "close",
        "END",
        "end",
        "BYE",
        "bye",
        "LOAD_LICENSE",
        "load_license",
        "CONFIG",
        "CONF",
        "CFG",
        "config",
        "conf",
        "cfg",
    ]
    options = [i for i in options if i.startswith(text)]

    if state < len(options):
        return options[state]
    else:
        return None


def main():
    readline.parse_and_bind("tab: complete")
    readline.set_completer(completer)

    arg_parser = ArgumentParser()

    arg_parser.add_argument("--host", "-H", type=str, help="Server host",
                            default="localhost")
    arg_parser.add_argument("--port", "-p", type=int, help="Server port",
                            default=6923)
    arg_parser.add_argument("--cert", "-c", type=str, help="Certificate file",
                            default=None)
    arg_parser.add_argument("--exec", "-e", nargs="+", type=str,
                            help="Pass a command", default=None)

    args = arg_parser.parse_args()

    if name == "nt":
        args.host = input("Enter arikedb host (localhost): ") or "localhost"
        args.port = int(input("Enter arikedb port (6923): ") or 6923)
        args.cert = input("Enter arikedb cert file path (NULL): ") or None

    cli = ArikedbClient(args.host, args.port, args.cert)

    cli.connect()

    hist_dir = environ.get("SNAP_USER_DATA", environ.get("HOME"))
    hist_file = None
    if hist_dir:
        hist_file = path.join(hist_dir, ".arike-cmd-hist")
        if path.isfile(hist_file):
            readline.read_history_file(hist_file)

    print(f"Connected to Arikedb Server on {args.host}:{args.port}")
    print(f"Arikedb CLI version {__version__}")

    if args.exec:
        for command in args.exec:
            cli.handle_response(cli.send_command(command.strip()))
        return

    flag = True

    while flag:
        try:
            cli_input = input(">> ")
            readline.add_history(cli_input)
            len_ = readline.get_current_history_length()
            if name != "nt":
                readline.remove_history_item(len_ - 1)
            if cli_input.lower().startswith("add_user"):
                pw = getpass("Create a password: ")
                cli_input += f" {pw}"
            elif cli_input.lower().startswith("auth"):
                pw = getpass("Password: ")
                cli_input += f" {pw}"

            rc = cli.handle_response(cli.send_command(cli_input))

            if rc == 1:
                break
        except KeyboardInterrupt:
            print("\r| Bye")
            break
        except Exception as err:
            print(f"Unexpected error: {err}")
            continue
        finally:
            if hist_file:
                readline.write_history_file(hist_file)

    if name == "nt":
        time.sleep(5)


if __name__ == '__main__':
    main()
